package env;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.ErrorHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverUtil {
    public static long DEFAULT_WAIT = 20;
    protected static WebDriver driver;

    public static WebDriver getDefaultDriver() {
        if (driver != null) {
            return driver;
        }
        System.setProperty("webdriver.chrome.driver", "tools/webdriver/chromedriver");
        System.setProperty("webdriver.gecko.driver", "tools/webdriver/geckodriver");

        try {
            driver = chooseDriver();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.manage().timeouts().setScriptTimeout(DEFAULT_WAIT, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    /**
     * By default to web driver will be PhantomJS
     *
     * Override it by passing -DWebDriver=Chrome to the command line arguments
     * 
     * @param capabilities
     * @return
     */
    private static WebDriver chooseDriver() throws MalformedURLException {
        String browser = System.getProperty("browser", "Firefox");
        String seleniumHubUrl = System.getProperty("selenium_hub_url");
        boolean headless = System.getProperty("Headless", "true").equals("true");
        DesiredCapabilities capabilities = null;

        switch (browser.toLowerCase()) {
            case "chrome":
                capabilities = DesiredCapabilities.chrome();
                capabilities.setJavascriptEnabled(true);
                capabilities.setCapability("takesScreenshot", true);
                if (seleniumHubUrl == null) {
                    final ChromeOptions chromeOptions = new ChromeOptions();
                    if (headless) {
                        chromeOptions.addArguments("--headless");
                    }
                    capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                    ChromeDriver chrome = new ChromeDriver();
                    ErrorHandler handler = new ErrorHandler();
                    handler.setIncludeServerErrors(false);
                    chrome.setErrorHandler(handler);
                    return chrome;
                } else {
                    return new RemoteWebDriver(new URL(seleniumHubUrl), capabilities);
                }
            case "firefox":
                capabilities = DesiredCapabilities.firefox();
                capabilities.setJavascriptEnabled(true);
                capabilities.setCapability("takesScreenshot", true);
                if (seleniumHubUrl == null) {
                    FirefoxOptions options = new FirefoxOptions();
                    if (headless) {
                        options.addArguments("-headless", "-safe-mode");
                    }
                    capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
                    final FirefoxDriver firefox = new FirefoxDriver();
                    return firefox;
                } else {
                    return new RemoteWebDriver(new URL(seleniumHubUrl), capabilities);
                }

            default:
                System.out.println("Invalid browser name:" + browser);
                System.exit(0);
                return null;
        }
    }

    public static WebElement waitAndGetElementByCssSelector(WebDriver driver, String selector, int seconds) {
        By selection = By.cssSelector(selector);
        return (new WebDriverWait(driver, seconds)).until(
                ExpectedConditions.visibilityOfElementLocated(selection));
    }

    public static void closeDriver() {
        if (driver != null) {
            try {
                driver.close();
                driver.quit(); 
            } catch (NoSuchMethodError nsme) {
            } catch (NoSuchSessionException nsse) {
            } catch (SessionNotCreatedException snce) {
            }
            driver = null;
        }
    }
}
